<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Auth;
use Validator;
use App\Models\User;

class AuthController extends Controller
{
    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        $token = $user->createToken('auth_token')->plainTextToken;

        return response()
            ->json(['data' => $user, 'access_token' => $token, 'token_type' => 'Bearer',]);
    }

    public function login(Request $request)
    {
        if (!Auth::attempt($request->only('email', 'password'))) {
            return response()
                ->json(['message' => 'Unauthorized'], 401);
        }

        $user = User::where('email', $request['email'])->firstOrFail();

        $token = $user->createToken('auth_token')->plainTextToken;

        return response()
            ->json(['message' => 'Hi ' . $user->name . ', welcome to home', 'access_token' => $token, 'token_type' => 'Bearer',]);
    }

    public function getUser($id = null)
    {
        if ($id) {
            $data = User::find($id);
            if ($data) {
                return response()->json($data);
            } else {
                return response()->json('No user Found');
            }
        } else {
            return response()->json(['data' => auth()->user()]);
        }
    }

    public function getAllUser()
    {
        $data = User::all();
        return response()->json(['data' => $data]);
    }

    // method for user logout and delete token
    public function logout(Request $request)
    {
        // return response()->json((['data'=> auth()->user()->tokens() ]));
        // auth()->user()->tokens()->delete();

        $user = $request->user();
        $user->currentAccessToken()->delete();

        return [
            'message' => 'You have successfully logged out and the token was successfully deleted'
        ];
    }

    public function getAuth(Request $request)
    {
        // Check if the user is authenticated with a valid access token
        if ($request->user()) {
            // User is authenticated with a valid access token
            $user = $request->user();
            $token = $request->bearerToken(); 
            return response()->json([
                'message' => 'Welcome, ' . $user->name, 
                'data'=> $user,
                'token' => $token
            ], 200);
        } else {
            // User is not authenticated or invalid token
            return response()->json(['message' => 'Unauthorized'], 401);
        }
    }
}
