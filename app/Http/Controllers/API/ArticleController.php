<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\ArticleService;

class ArticleController extends Controller
{
    private $articleService;
    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService;
        
    }

    public function index(){
        $data = $this->articleService->getAll();
        return response()->json($data);
    }
    public function create(Request $request){
        $data = $this->articleService->create($request);
        return response()->json($data);
    }

    public function delete(Request $request){
        $data = $this->articleService->delete($request->id);
        return response()->json($data);
    }

}
