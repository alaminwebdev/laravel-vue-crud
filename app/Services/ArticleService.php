<?php

namespace App\Services;

use App\Models\Article;
use App\Services\IService;
use Illuminate\Http\Request;

/**
 * Class ArticleService
 * @package App\Services
 */

class ArticleService implements IService
{
    public function getAll()
    {
        $data                       = Article::get();
        return $data;
    }

    public function getByID($id)
    {
        $data                       = Article::findOrFail($id);
        return $data;
    }

    public function create(Request $request)
    {
        try {
            $data                   = new Article();
            $data->title            = $request->title;
            $data->description      = $request->description;
            
            $data->save();

            return $data;
        } catch (Exception $e) {
            return $e;
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $data                   = Article::findOrFail($id);
            $data->name             = $request->name;
            $data->status           = $request->status;
            $image                  = $request->file('image');

            if ($image) {
                @unlink(public_path('upload/logo/' . $data->image));
                $imageName          = date('Ymd') . time() . '.'  . $image->getClientOriginalExtension();
                $image->move(public_path('upload/logo'), $imageName);
                $data->image        = $imageName;
            }
            $data->update();

            return true;
        } catch (Exception $e) {
            return $e;
        }
    }

    public function delete($id)
    {
        $data                       = Article::findOrFail($id);
        $data->delete();
        return $data;
    }
}
