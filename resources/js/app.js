import './bootstrap';
import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import App from './components/App.vue'; // Import the App component
import Home from './components/Home.vue';
import About from './components/About.vue';
import ArticleList from './components/ArticleList.vue';

const router = createRouter({
    history: createWebHistory(),
    routes:[
        {
            path:'/',
            name:'home',
            component:Home
        },
        {
            path:'/about',
            name:'about',
            component:About
        },
        {
            path:'/articles',
            name:'article',
            component:ArticleList
        }
    ],
});

const app = createApp(App); // Pass the App component to createApp

app.use(router).mount('#app');