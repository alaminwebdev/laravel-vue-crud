<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\Api\ArticleController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

//API route for register new user
Route::post('/register', [AuthController::class, 'register']);
//API route for login user
Route::post('/login', [AuthController::class, 'login']);

Route::prefix('article')->name('article.')->group(function (){
    Route::get('/list', [ArticleController::class, 'index']);
    Route::post('/create', [ArticleController::class, 'create']);
    Route::post('/delete', [ArticleController::class, 'delete']);
    // Route::post('/update/{id}', [ArticleController::class, 'update']);
    // Route::get('/{id}', [ArticleController::class, 'gerArticle']);
    // Route::get('/{numbers}', [ArticleController::class, 'gerArticles']);
});


Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/user/{id?}', [AuthController::class, 'getUser']);
    Route::get('/all-user', [AuthController::class, 'getAllUser']);


    Route::get('/get-auth', [AuthController::class, 'getAuth']);
    
    //API route for logout user
    Route::post('/logout', [AuthController::class, 'logout']);

    

});
